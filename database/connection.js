const mongoose = require('mongoose');
const UserModel = require('../model/model')


const connectDB = async() => {
    try{
            const con = await mongoose.connect(process.env.MONGO_URL,{
                useUnifiedTopology:true,
            }) 
            console.log('MongoDB connected');
    }catch(err){
        console.log(err);
    }
}

module.exports =   connectDB  