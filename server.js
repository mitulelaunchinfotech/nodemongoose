const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const md5 = require('md5');
require('dotenv').config()
const nodeMailer = require('nodemailer');


// Router Prefix Setup 
express.application.prefix = express.Router.prefix = function(path, configure) {
    var router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
};

const connectDB = require('./database/connection')

const app = express();

dotenv.config({path:'.env'})
const PORT = process.env.PORT || 8000;

app.use(morgan('tiny'));

connectDB();

app.use(bodyParser.urlencoded({extended:true}))

app.use(express.static('public'));

app.set("view engine","ejs")

//load assets
app.use('/css',express.static(path.resolve(__dirname,"views/ejs")))

app.use('/',require('./routes/router'))



app.listen(3000,()=>{ console.log('server running')})

