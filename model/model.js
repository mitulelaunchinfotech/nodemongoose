const mongoose = require('mongoose');
var dataTables = require('mongoose-datatables');   
var Schema = mongoose.Schema;

var schema = new Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
    },
    password:{
        type:String,
        required:true,
    },
    gender:String,
    status:String,
    tokenData:{ 
        type:String,
        default:null,
    },
    image:{
        type:String,
		allowNull: true,
        data:Buffer,contentType: String
    },
    class_id:  {
         type:mongoose.Schema.Types.ObjectId,ref:'classdbs'
        } ,
    otp:String,
})
schema.plugin(dataTables)

const Userdb = mongoose.model('userdb',schema);

module.exports = Userdb;