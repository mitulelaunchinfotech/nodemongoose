var Userdb = require('../model/model')
const md5 = require('md5');
const multer = require('multer');
const nodeMailer = require('nodemailer');
const mongoose = require('mongoose');

const {
    createErrorResponse,
    createSuccessResponse
} = require('../helpers/responseweb');
const req = require('express/lib/request');

exports.create = async (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty"
        });
        return;
    }
    const user = new Userdb({
        name: req.body.name,
        email: req.body.email,
        password: md5(req.body.password),
        gender: req.body.gender,
        image: req.body.image,
        status: req.body.status,
        class_id: req.body.class_id
    })

    user
        .save(user)
        .then(async (data) => {
            const user1 = await Userdb.aggregate([{
                    $match: {
                        _id: mongoose.Types.ObjectId(data._id)
                    },
                },
                {
                    $lookup: {
                        from: "classdbs",
                        localField: "class_id",
                        foreignField: "_id",
                        as: "anything"
                    }
                }
            ])
            res.send(user1)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.find = (req, res) => {
    Userdb.find().then(user => {
            res.send(user)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty"
        })
    }

    const id = req.params.id;
    Userdb.findByIdAndUpdate(id, req.body, {
            useFindAndModify: false
        })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: 'Cannot Update user'
                })
            } else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.delete = (req, res) => {
    const id = req.params.id;
    Userdb.findByIdAndDelete(id).then(data => {
            if (!data) {
                res.status(404).send({
                    message: 'Cannot delete user'
                })
            } else {
                res.send({
                    message: "User delete succesfully"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.imageUpload = (req, res) => {
    try {
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, './public/assets/upload')
            },
            filename: function (req, file, cb) {
                cb(null, Date.now() + '_' + file.originalname)
            }
        })

        var upload = multer({
            storage: storage
        }).single('image')

        upload(req, res, async (err) => {
            res.send({
                message: req.file.filename
            })
        });

    } catch (err) {
        console.log(err);
    }
}

exports.resetPassword = async (req, res) => {
    try {
        const {
            email,
            password,
        } = req.body;

        if (!email) {
            return createErrorResponse(req, res, 'Enter email', {}, 422)
        }
        if (!password) {
            return createErrorResponse(req, res, 'Enter password', {}, 422)
        }

        var userData = await Userdb.findOne({
            email: email
        }).then((result) => {
            return result;
        });

        if (userData) {
            let update = await Userdb.updateOne({
                email: email
            }, {
                password: await md5(password)
            });
            console.log(update);
            res.send({
                message: update
            })
        } else {
            return createErrorResponse(req, res, 'Email not found', {}, 422);
        }
    } catch (err) {
        console.log(err);
    }
}

exports.forgetPassword = async (req, res) => {
    const {
        email,
    } = req.body;

    var userData = await Userdb.findOne({
        email: email
    }).then((result) => {
        return result;
    });


    if (userData) {
        let otp = Math.floor(100000 + Math.random() * 900000);
        var transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: "mitullukka07@gmail.com",
                pass: "mitul7600"
            }
        });

        var mailOptions = {
            from: 'mitullukka07@gmail.com',
            to: req.body.email,
            subject: 'Hello,',
            text: 'OTP sended to your registered email' + otp
        }
        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {

            } else {

                let update = await Userdb.updateOne({
                    email: email
                }, {
                    otp: otp
                });

                return res.status(200).send({
                    message: "OTP sent on email"
                })
            }
        })

    } else {
        console.log('Not found');
    }
}


exports.verfiyOtp = async (req, res) => {
    const {
        email,
        otp
    } = req.body;

    var userData = await Userdb.findOne({
        email: email
    }).then((result) => {
        return result;
    });

    if (userData) {
        let update = await Userdb.updateOne({
            email: email
        }, {
            otp: ''
        });

        if (userData.otp == "") {
            return createSuccessResponse(res, 'Email and otp already verify', {});
        }

        return createSuccessResponse(res, 'Email and otp match', {});

    } else {
        return createErrorResponse(req, res, 'Email or otp not match', {}, 422);
    }
}

exports.changePassword = async (req, res) => {
    const {
        email,
        password,
        newpassword
    } = req.body;

    var userPassword = await Userdb.findOne({
        email: email
    }).then((result) => {
        return result;
    });

    if (userPassword.password == md5(password)) {
        if (userPassword) {
            let updatePassword = await Userdb.updateOne({
                email: email
            }, {
                password: md5(newpassword)
            });
            return createSuccessResponse(res, 'Password change succesfully', {});
        }
    } else {
        return createErrorResponse(req, res, 'Old password not match', {}, 422);
    }
}