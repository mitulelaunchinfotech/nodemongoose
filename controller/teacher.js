var Teacherdb = require('../models/Teacher')

exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: 'Content can not be empty'
        })
        return;
    }

    const teacher = new Teacherdb({
        name: req.body.name,
        email: req.body.email,
        address: req.body.address
    })

    teacher.save(teacher).then(data => {
            res.send(data)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.getteacher = (req, res) => {
    Teacherdb.find().then(teacher => {
            res.send(teacher)
        })
        .catch(err => {
            res.status(500).send({
                message: error.message || "error"
            })
        });
}