var Subjectdb = require('../models/Subject')

exports.create = (req,res)=>{
    if(!req.body)
    {
        res.status(400).send({
            message:'Content can not be empty'
        })
        return;
    }

    const subject = new Subjectdb({
        Sub_Id : req.body.Sub_Id,
        name : req.body.name
    })

    subject.save(subject).then(data=>{
        res.send(data)
    })
}