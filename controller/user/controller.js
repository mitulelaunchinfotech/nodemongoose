var Userdb = require('../../model/model')
const md5 = require('md5');


exports.create = async (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty"
        });
        return;
    }
    const user = new Userdb({
        name: req.body.name,
        email: req.body.email,
        password: md5(req.body.password),
        gender: req.body.gender,
        image : req.body.image,
        status: 1,
        class_id : req.body.class_id
    })
    
    

    user
        .save(user)
        .then(data => {
            res.redirect('add-user')
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.find = (req, res) => {
    Userdb.find().then(user => {
            res.send(user)
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty"
        })
    }

    const id = req.params.id;
    Userdb.findByIdAndUpdate(id, req.body, {
            useFindAndModify: false
        })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: 'Cannot Update user'
                })
            } else {
                res.send(data)
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}

exports.delete = (req, res) => {
    const id = req.params.id;
    Userdb.findByIdAndDelete(id).then(data => {
            if (!data) {
                res.status(404).send({
                    message: 'Cannot delete user'
                })
            } else {
                res.send({
                    message: "User delete succesfully"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}


exports.getuser = (async (req,res)=> {
    Userdb.dataTable(req.query, function (err, data) {
        res.send(data);
      });
});

// module.exports = getuser;
