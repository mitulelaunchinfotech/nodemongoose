var Classdb = require('../models/Class');
var mongoose = require('mongoose')
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty"
        });
        return;
    }
    const user = new Classdb({
        name: req.body.name,
        teacher_id:req.body.teacher_id,
        subject_id:req.body.subject_id
     })
    user
        .save(user)
        .then(async (data) => {
            const user1 = await Classdb.aggregate([{
                    $match: {
                        _id: mongoose.Types.ObjectId(data._id)
                    },
                },
                {
                    $lookup: {
                        from: "teachers",
                        localField: "teacher_id",
                        foreignField: "_id",
                        as: "teacher"
                    }                  
                },
                {
                    $lookup: {
                        from: "subjects",
                        localField: "subject_id",
                        foreignField: "_id",
                        as: "subject"
                    }
                }
            ])
             res.send(user1)
            })
        .catch(err => {
            res.status(500).send({
                message: err.message || "error"
            })
        })
}


exports.getclass = (req, res) => {
    Classdb.find().then(user => {
            res.send(user)
        })
        .catch(err => {
            res.status(500).send({
                message: error.message || "error"
            })
        });
}
