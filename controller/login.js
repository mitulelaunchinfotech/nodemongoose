const express = require('express');
const md5 = require('md5');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

const models = require("../model/model")

const {

} = require('../database/connection')


const {
    createSuccessResponse,
    createErrorResponse
} = require("../helpers/responseweb");
const Userdb = require('../model/model');

const userlogin = (async (req, res) => {
    const {
        email,
        tokendata,
        password
    } = req.body;

    userInfo = await models.findOne({
        email: email,
    }).then((userData) => {
        userInfo = userData;
        return userData;
    });

    if (userInfo == null) {
        return createErrorResponse(req, res, 'Email not found', {
            "email": "Email not found"
        }, 422);
    } else {

        let tokendata = md5(Math.floor(Math.random() * 9000000000) + 1000000000) + md5(new Date(new Date().toUTCString()));
        
        userInfo.tokenData = tokendata;
        await userInfo.save();

        let token = jwt.sign({
            tokendata
        }, process.env.secret, {
            expiresIn: '24h'
        });

        response = {
            user_id: userInfo.id,
            name: userInfo.name,
            gender: userInfo.gender,
            email: userInfo.email,
            token: token,
        }
        return createSuccessResponse(res, "Login Successfully", response);
    }
})

const logout = (async(req,res)=>{

    const userInfo = req.decoded;
    var uid = userInfo._id;
    
    // let userupdatdata;
    // userupdatdata = {
    //     tokenData: '',
    // }

    userupdatInfo = await models.updateOne({ _id:uid }, { tokenData: '' });

    if(req.session){
        req.session.destroy();
    }
    return createSuccessResponse(res, "Logout Successfully", {});
});

module.exports = {
    userlogin,
    logout
}