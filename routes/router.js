const express = require('express')

const router = express.Router();

const { route } = require('express/lib/application');


const services = require('../services/render')
const controller = require('../controller/controller')
const ClassController = require('../controller/class')
const TeacherController = require('../controller/teacher')
const SubjectController = require('../controller/subject')

const AdminUserController = require('../controller/user/controller');
const LoginControllerAPI = require('../controller/login')
const verifyToken = require('../middlewares/index');


//API
router.prefix('/api', (route) => {

    route.get('/',services.homeRoutes);
    route.get('/add-user',services.add_user)
    route.get('/get-user',AdminUserController.getuser)

    route.post('/user-login',LoginControllerAPI.userlogin);
    route.post('/logout',verifyToken,LoginControllerAPI.logout);

    route.post('/api/users',verifyToken,controller.create);
    route.post('/image-upload',verifyToken,controller.imageUpload);
    route.post('/reset-password',verifyToken,controller.resetPassword);
    route.post('/forget-password',verifyToken,controller.forgetPassword);
    route.post('/verify-otp',verifyToken,controller.verfiyOtp);
    route.post('/change-password',verifyToken,controller.changePassword);

    route.get('/api/users',controller.find);
    route.post('/api/users/:id',controller.update);
    route.delete('/api/users/:id',controller.delete);


    //Class
    route.post('/add-class',verifyToken,ClassController.create)
    route.get('/get-class',verifyToken,ClassController.getclass)

    //Teacher
    route.post('/add-teacher',verifyToken,TeacherController.create)
    route.get('/get-teacher',verifyToken,TeacherController.getteacher)


    //Subject
    route.post('/add-subject',SubjectController.create)


});

module.exports = router;