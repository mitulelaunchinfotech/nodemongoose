const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name:{
        type:String,
        required:true,
        unique:[true, 'A name exists. The email must be unique.'],
    },
    teacher_id:{
      type:mongoose.Schema.Types.ObjectId,ref:'teachers'
    },
    subject_id:{
      type:mongoose.Schema.Types.ObjectId,ref:'subjects'
    },
})

const Classdb = mongoose.model('classdb',schema);

module.exports = Classdb;
