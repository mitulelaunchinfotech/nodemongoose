const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
  Sub_Id: {
      type: String
    },
    name: {
      type: String,
      required: true
    }
})

const Subjectdb = mongoose.model('subject',schema);

module.exports = Subjectdb;