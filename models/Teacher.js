// 'use strict';
// module.exports = mongoose => {
//   const newSchema = new mongoose.Schema({
//     name: {
//       type: String
//     },
//     email: {
//       type: String
//     },
//     address: {
//       type: String
//     }
//   }, {
//     timestamps: {
//       createdAt: 'created_at',
//       updatedAt: 'updated_at'
//     }
//   });
//   const Teacher = mongoose.model('Teacher', newSchema);
//   return Teacher;
// };

const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    name:{
        type:String,
        required:true
    },
    email:{
      type:String,
      required:true
    },
    address:{
      type:String,
    }
})

const Teacherdb = mongoose.model('teacher',schema);

module.exports = Teacherdb;